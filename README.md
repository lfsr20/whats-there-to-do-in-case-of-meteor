# Behold the most innovative and amazing Application that your eyes ever had the pleasure of seeing

From one of the developers of the now infamous KILLERapp: Here's the next project that will conquer the world like the mongols did in the middle ages - but peacefully: An App that let's you manage a __to-do-list__. 

Pretty much impossible to not be excited, right? <br>
So going back to the mongolians back in the middle-ages: they would have never forgotten to conquer Europe ... or Africa or the Americas ... because they'd actually have been reminded to do so by this App. And so will you. Never again will you forget to conquer the Americas - or any task that needs to be done for that matter. 
This Apps usefulness is truely immeasurable.

It's built by following a Tutorial about creating an App using the Meteor Framework and __Angular__. It's pretty *amazing* actually.