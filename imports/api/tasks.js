/*
in order to create a collection we define this new module, which creates a Mongo collection
and exports it!
It's placed in a new 'api'-folder in imports, that stores api-related imports.
as in: other collections as well as methods and publications
 */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Tasks = new Mongo.Collection('tasks');

if(Meteor.isServer){
    //this code only runs on the server
    /*
    calling Meteor.publish on the server registers a publication named 'name'
    When $scope.subscribe is called on the client with the publication name, the client subscribes
    to all data from that publication, which in this case is all of the tasks in the database
     */
    Meteor.publish('tasks', function tasksPublication() {
        return Tasks.find({
            $or: [{
                private: {
                    $ne: true
                }
            }, {
                owner: this.userId
            }, ],
        });
    });
}

Meteor.methods({
    'tasks.insert' (text) {
        check(text, String);

        //make sure the user is logged in before inserting a task
        if(!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        Tasks.insert({
            text,
            createdAt: new Date(),
            owner: Meteor.userId(),
            username: Meteor.user().username,
        });
    },
    'tasks.remove' (taskId) {
        check(taskId, String);

        const task = Tasks.findOne(taskId);
        /*
        if only the owner of a task should be able to delete it: delete task.private-part of if
         */
        if(task.private && task.owner !== Meteor.userId()) {
            //if the task is private, make sure only the owner can delete it!
            throw new Meteor.Error('not-authorized');
        }

        Tasks.remove(taskId);
    },
    'tasks.setChecked' (taskId, setChecked) {
        check(taskId, String);
        check(setChecked, Boolean);

        const task = Tasks.findOne(taskId);
        if(task.private && task.owner !== Meteor.userId()){
            //if task is private, mal sure only the owner can check it off
            throw new Meteor.Error('not-authorized');
        }


        Tasks.update(taskId, {
            $set: {
                checked: setChecked
            }
        });
    },
    'tasks.setPrivate' (taskId, setToPrivate){
        check(taskId, String);
        check(setToPrivate, Boolean);

        const task = Tasks.findOne(taskId);

        //Make sure only the task owner can make a task private
        if(task.owner !== Meteor.userId()) {
            throw new Meteor.Error('not authorized');
        }

        Tasks.update(taskId, {
            $set: {
                private: setToPrivate
            }
        });
    },
});

/*
COLLECTIONS
Meteor's way of storing persistent data. The can be accessed by server AND client.
The update themeselves automatically, so a view component backed by a collection will
automatically display the most up-to-date data.

 Creating a collection
 is done by calling 'new Mongo.Collection("collection_name") in the javascript file.
 This sets ip a MongoDB called "collection_name" on the server.
 On the client it creates a cache connected to the server collection.

DOCUMENTS: Items inside collections.
 */