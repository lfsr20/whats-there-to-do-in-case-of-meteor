import angular from 'angular';
import angularMeteor from 'angular-meteor';
import { Meteor } from 'meteor/meteor';
import { Tasks } from '../../api/tasks.js';

import template from './todosList.html';

class TodosListCtrl {
    constructor($scope) {
        $scope.viewModel(this);

        this.subscribe('tasks');
        this.hideCompleted = false;

        this.helpers({
            tasks() {
                const selector = {};

                //if hide completed is checked, filter tasks
                //getReactively turns Angular scope variables into Meteor reactive variables.
                if(this.getReactively('hideCompleted')) {
                    selector.checked = {
                        $ne: true
                    };
                }

                //show newest task at the top
                return Tasks.find(selector, {
                    sort: {
                        createdAt: -1
                    }
                });
            },
            incompleteCount() {
                return Tasks.find({
                    checked: {
                        $ne: true
                    }
                }).count();
            },
            currentUser() {
                return Meteor.user();
            }
        })
    }

    addTask(newTask) {
        //Insert a task into the collection
        Meteor.call('tasks.insert', newTask);
        /*
        Before:
        Tasks.insert({

            text: newTask,
            createdAt: new Date,
            owner: Meteor.userId(),
            username: Meteor.user().username
        });
        */

        /*
        by default the inserted properties of the task objects are not defined. you could add
        an object with very different properties without problem. Not really secure!
         */

        //clear form
        this.newTask = '';
    }

    setChecked(task) {
        //set the checked property to the opposite of its current value
        Meteor.call('tasks.setChecked', task._id, !task.checked);

        /*
        Tasks.update(task._id, {
            $set: {
                checked: !task.checked
            },
        });
        */
    }

    removeTask(task) {
        Meteor.call('tasks.remove', task._id);
        // Tasks.remove(task._id);
    }

    setPrivate(task) {
        Meteor.call('tasks.setPrivate', task._id, !task.private);
    }

}

export default angular.module('todosList', [
    angularMeteor
])
  .component('todosList', {
      templateUrl: 'imports/components/todosList/todosList.html',
      controller: ['$scope', TodosListCtrl]
  });